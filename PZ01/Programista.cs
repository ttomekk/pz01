﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ01
{
    class Programista : Pracownik
    {
        public int technologie { get; set; }

        public Programista(int ID, string imie, string nazwisko, bool staz, int pensja, int technologie):base(ID, imie, nazwisko, staz, pensja)
        {
            this.technologie = technologie;
        }

        public override string ToString()
        {
            return base.ToString() + $" Liczba technologii: {this.technologie}";
        }

        public override void wprowadz()
        {
            base.wprowadz();
            Console.Write("Wprowadz liczbe technologii");
            this.technologie = Convert.ToInt32(Console.ReadLine());
        }

        public Programista()
        {

        }

        public override void podwyzka(int pensja)
        {
            base.podwyzka(pensja * technologie);
        }
    }
}
