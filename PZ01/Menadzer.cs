﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ01
{
    class Menadzer : Pracownik
    {
        public int liczbaProjektow { get; set; }

        public Menadzer(int ID, string imie, string nazwisko, bool staz, int pensja, int liczbaProjektow):base(ID, imie, nazwisko, staz, pensja)
        {
            this.liczbaProjektow = liczbaProjektow;
        }

        public override string ToString()
        {
            return base.ToString() + $" Liczba projektow: {this.liczbaProjektow}";
        }

        public override void wprowadz()
        {
            base.wprowadz();
            Console.WriteLine("Wprowadz liczbe projektow:");
            this.liczbaProjektow = Convert.ToInt32(Console.ReadLine());
        }
        public Menadzer()
        {

        }

        public override void podwyzka(int pensja)
        {
            base.podwyzka(pensja * liczbaProjektow);
        }
    }
}
