﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ01
{
    static class Zarzad
    {
        private static List<Pracownik> lista = new List<Pracownik>();

        public static void wyswietl()
        {
            foreach (Pracownik p in lista)
            {
                Console.WriteLine(p.ToString());
            }
        }

        public static void dodajPrac(Pracownik p)
        {
            lista.Add(p);
        }
    }
}
