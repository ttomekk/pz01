﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ01
{
    abstract class Pracownik
    {
        public int ID { get; set; }
        public string imie { get; set; }
        public string nazwisko { get; set; }
        public bool staz { get; set; }
        public int pensja { get; set; }

        public Pracownik()
        {
            ID = 0;
            imie = "";
            nazwisko = "";
            staz = false;
            pensja = 0;
        }

        public Pracownik(int ID, string imie, string nazwisko, bool staz, int pensja)
        {
            this.ID = ID;
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.staz = staz;
            this.pensja = pensja;
        }

        public override string ToString()
        {
            return String.Format($"Imie: {this.imie} Nazwisko: {this.nazwisko} Staż: {this.staz} Pensja: {this.pensja}");
        }

        public virtual void wprowadz()
        {
            Console.WriteLine("Podaj imie:");
            this.imie = Console.ReadLine();
            Console.WriteLine("Podaj nazwisko:");
            this.nazwisko = Console.ReadLine();
            Console.WriteLine("Podaj staz:");
            this.staz = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Podaj pensje:");
            this.pensja = Convert.ToInt32(Console.ReadLine());
        }

        public virtual void podwyzka(int pensja)
        {
            this.pensja += pensja;
        }
    }
}
