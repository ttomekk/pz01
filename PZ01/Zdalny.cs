﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ01
{
    class Zdalny:Programista
    {
        public int odleglosc { get; set; }

        public Zdalny(int ID, string imie, string nazwisko, bool staz, int pensja, int technologie, int odleglosc):base(ID, imie, nazwisko, staz, pensja, technologie)
        {
            this.odleglosc = odleglosc;
        }

        public override string ToString()
        {
            return base.ToString() + $" Odleglosc: {this.odleglosc}";
        }

        public override void wprowadz()
        {
            base.wprowadz();
            Console.Write("Wprowadz odleglosc");
            this.odleglosc = Convert.ToInt32(Console.ReadLine());
        }

        public Zdalny()
        {

        }

        public override void podwyzka(int pensja)
        {
            base.podwyzka(pensja * odleglosc);
        }
    }
}
