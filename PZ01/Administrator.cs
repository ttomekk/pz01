﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ01
{
    class Administrator : Pracownik
    {
        public int liczbaSerwerow { get; set; }

        public Administrator(int ID, string imie, string nazwisko, bool staz, int pensja, int liczbaSerwerow):base(ID, imie, nazwisko, staz, pensja)
        {
            this.liczbaSerwerow = liczbaSerwerow;
        }

        public override string ToString()
        {
            return base.ToString() + $" Liczba serwerow: {this.liczbaSerwerow}";
        }

        public override void wprowadz()
        {
            base.wprowadz();
            Console.Write("Wprowadz liczbe serwerow");
            this.liczbaSerwerow = Convert.ToInt32(Console.ReadLine());
        }

        public Administrator()
        {

        }

        public override void podwyzka(int pensja)
        {
            base.podwyzka(pensja * liczbaSerwerow);
        }
    }
}
